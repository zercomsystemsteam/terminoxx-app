import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import ManagerLogin from './Pages/ManagerLogin';
import Adminlogin from './Pages/Adminlogin';
import {BrowserRouter as Router, Route, Link, Switch} from "react-router-dom";
import { Provider } from 'react-redux';
import store from './redux/store';

/* import reportWebVitals from './reportWebVitals'; */
import PublicRoute from './includes/PublicRoute';


ReactDOM.render(
  <React.StrictMode>
   <Router>
      <Switch>
         <Route>
         <Provider store={store}>
          <App />
         </Provider>
         </Route>
         <PublicRoute>
            <Adminlogin/>
         </PublicRoute>
         <Route>
            <ManagerLogin/>
         </Route>
      </Switch>
   </Router>
  </React.StrictMode>,
  document.getElementById('root')
);