import React from 'react'
import terminoxlogo from "../logo2old.png";
import "../App.css";
import avatar5 from "../picture.png";
import { Link} from 'react-router-dom';

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Nav from './Nav';
import DoughnutChart from '../DoughnutChart'
import LineChart from '../LineChart';
import {Formik, Field, Form,FormikProps} from 'formik'

function AddAtm() {
  
    return (
        <div>    
            <div className="main-navigation">
                <div className="left-navigation-link">
                    <Nav/> 
                </div>  
                <div className="right-navigation-link-atm">
                    <p className="dashoboard-text mb-10">Add New ATM</p>

                    <div className="Add-atm-form">
                        <Formik>
                            <Form>
                                <div className="Long-formik-input">
                                <label>Atm ID:</label>
                                    <Field autoComplete="off" name="ATM ID"/>
                                </div>
                                 <div className="divide-one-formik-input">
                                    <div className="model-make">
                                        <label>Model:</label> <br/>
                                            <Field autoComplete="off" name="ATM ID"/>
                                    </div>

                                     <div className="model-make">
                                        <label>Make:</label> <br/>
                                        <Field autoComplete="off" name="ATM ID"/>
                                    </div>

                                    
                                </div>

                                  <div className="divide-one-formik-input">
                                    <div className="model-make">
                                        <label>Year:</label> <br/>
                                            <Field component="select" name="ATM ID">
                                                <option>2012</option>
                                            </Field>
                                     </div>

                                     <div className="model-make">
                                        <label>Age:</label> <br/>
                                        <Field component="select" name="ATM ID">
                                                <option></option>
                                        </Field>
                                    </div>

                                    <div className="model-make">
                                        <label>Manufacture Year:</label> <br/>
                                        <Field component="select" name="ATM ID">
                                                <option></option>
                                        </Field>
                                    </div>                                    
                                </div>

                                <div className="Long-formik-input">
                                <label>ATM Current Status:</label>
                                    <Field component="select" name="ATM ID">
                                                <option></option>
                                     </Field>
                                </div>

                                <div className="Long-formik-input">
                                <label>ATM Address / Location:</label>
                                     <Field name="ATM ID"/>             
                                </div>

                                <div className="Long-formik-input">
                                <label>Religion:</label>
                                     <Field component="select" name="ATM ID">
                                                <option></option>
                                     </Field>
                                </div>

                                <div className="Long-formik-input">
                                <label>State:</label>
                                     <Field component="select" name="ATM ID">
                                                <option></option>
                                     </Field>
                                </div>

                                <div className="Long-formik-input">
                                <label>Branch:</label>
                                    <Field component="select" name="ATM ID">
                                                <option></option>
                                     </Field>
                                </div>

                                <div className="Long-formik-input">
                                <label>Vendor:</label>
                                     <Field component="select" name="ATM ID">
                                                <option></option>
                                     </Field>
                                </div>

                                <div className="Long-formik-input">
                                <label>Regional Managers:</label>
                                     <Field component="select" name="ATM ID">
                                                <option></option>
                                     </Field>
                                </div>

                                <div className="divide-one-formik-input">
                                    <div className="model-make">
                                        <label>Custodians:</label> <br/>
                                            <Field component="select" name="ATM ID">
                                                <option>Mr Johnson</option>
                                                <option>Mr Wale Johnson</option>
                                                <option>Mrs OluwaBusayo</option>
                                            </Field>
                                    </div>

                                     <div className="model-make">
                                        <label>Selected Custodians:</label> <br/>
                                        <Field autoComplete="off" name="ATM ID"/>
                                    </div>   
                                </div>
                                <div>
                                       <button className="bg-red-700 pt-4 pb-4 text-white w-80 mt-10 mb-10 rounded">Add ATM </button>
                                    </div> 

                                
                            </Form>
                        </Formik>
                    </div>
                </div>
            </div>
        </div>  
    )
}
export default AddAtm


