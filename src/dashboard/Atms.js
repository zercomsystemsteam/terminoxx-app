import React from 'react'
import "../App.css";
import axios from 'axios';
import { selectedAtm, selectedCustodian, selectedMedia, selectedVendor } from '../redux/actions/atmActions';
import { selectedDevice } from '../redux/actions/atmActions';
import { useParams } from 'react-router-dom';
import Nav from './Nav';
import DoughnutChart from '../DoughnutChart'
import LineChart from '../LineChart'
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { selectedCash } from '../redux/actions/atmActions';
import loader from '../Img/loader.gif';
import VendorPopup from './VendorPopup';
import { useState } from 'react';

const Atms = (props) => {
    const [buttonPopup, setButtonPopup] = useState(false);
    const dispatch = useDispatch();
    const getToken = sessionStorage.getItem("token");
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${getToken}`
    }
    const getSingleAtm = async () => {
        const response = await axios.get(`https://721f-38-107-67-161.ngrok.io/api/v1.1/atm_oracle/${atm_id}`, { headers }).catch((err) => {
            console.log("Err", err);

        });
        dispatch(selectedAtm(response.data.atm[0]));
        dispatch(selectedDevice(response.data.device));
        dispatch(selectedMedia(response.data.media));
        dispatch(selectedCash(response.data.cash[0]));
        dispatch(selectedCustodian(response.data.custodian));
        dispatch(selectedVendor(response.data.vendor[0]));
    }


    const vendorNotification = async () => {
        const sender = await axios.post(`https://721f-38-107-67-161.ngrok.io/api/v1.1/vendor_mail/${atm_id}`, { headers }).then((response) => {
            console.log(response.data);
            console.log(sender);
        }).catch((err) => {
            console.log("Err", err);
        });


    }

    const atms = useSelector((state) => state.atm);
    const { state_name, citystate, description, id } = atms;
    const { atm_id } = useParams();

    const Displaydevices = useSelector((state) => state.devices);
    const Displaymedias = useSelector((state) => state.medias);
    const Displaycashs = useSelector((state) => state.cashs);

    const Displayvendors = useSelector((state) => state.vendors);
    const Displaycustodians = useSelector((state) => state.custodians);

    const { curref, cassette_value, cassette_status, notes_initial, notes_left, notes_dispensed, notes_rejected, notes_adjusted, previous_notes_left, date_modified } = Displaycashs;
    const { company_name, firstname, lastname, email, phone, region, state, branch, created_at, updated_at } = Displayvendors;
    const vendorFullName={firstname} + {lastname}
    
    useEffect(() => {
        if (atm_id && atm_id !== "") getSingleAtm();
    }, [atm_id]);



    return (

        <div>

            {Object.keys(atms).length === 0 ? (
                <center>
                    <div>
                        <img src={loader} alt="loader image"/>
                    </div>
                </center>
            ) : (
                <div className="main-navigation">

                    <div className="left-navigation-link">
                        <Nav />
                    </div>
                    <div className="right-navigation-link-atm">
                        <p className="dashoboard-text">Dashboard </p>
                        <div className="dashboad-main-page">
                            <div className="dashoboard-line-one">
                                <div className="dashboard-atm-one">
                                    <div className="atm-offine">
                                        <span className="atm-name-name">ATM</span>
                                        <span className="offline-class">
                                            {state_name}</span>
                                    </div>
                                    <div className="atm-other-contents">
                                        <ul>
                                            <li><b>Terminal ID:</b>{atm_id}</li>
                                            <li><b>Address:</b> 27, Victoria Island, lagos, Nigeria</li>
                                            <li><b>Region:</b> </li>
                                            <li><b>Make:</b> </li>
                                            <li><b>State:</b> {citystate}</li>
                                            <li><b>Year:</b> </li>
                                            <li><b>Description: </b>{description}</li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                           
                            <div className="dashoboard-line-two">
                                <div className="dashboard-atm-two">
                                    <div className="atm-offine">
                                        <span className="custodian-name-name">Custodian</span>
                                        <span className="custodian-class"><button>Notify Custodian</button></span>
                                    </div>


                                    <div className="atm-other-contents" id="custodian-scroll">


                                        {
                                            Object.keys(Displaycustodians).length === 0 ? (
                                                <div>
                                                    No Custodian Allocated For this ATM

                                                </div>
                                            ) : (
                                                <div>     {




                                                    Object.keys(Displaycustodians).map(Displaycustodian => {
                                                        return (
                                                            <div key={Displaycustodian} className="device-scroll-detailes-one">

                                                                <div className="custodian_status">
                                                                    <span>FullName:</span> <span>{Displaycustodians[Displaycustodian].firstname}-{Displaycustodians[Displaycustodian].lastname}</span> <br />
                                                                    <span>Email:</span> <span>{Displaycustodians[Displaycustodian].email}</span><br />
                                                                    <span>Phone Number:</span> <span>{Displaycustodians[Displaycustodian].phone}</span><br />
                                                                    <span>Region:</span> <span>{Displaycustodians[Displaycustodian].region}</span><br />
                                                                    <span>State:</span> <span>{Displaycustodians[Displaycustodian].state}</span>
                                                                    <hr />
                                                                </div>

                                                                {/*  <div className="state-status">{Displaycustodians[Displaycustodian].lastname}</div> */}
                                                            </div>
                                                        )

                                                    })
                                                }
                                                </div>
                                            )

                                        }




                                    </div>



                                </div>
                            </div>
                            <div className="dashoboard-line-three">
                                <div className="dashboard-atm-three">
                                    <div className="atm-offine">
                                        <span className="vendor-name-name">Vendor</span>
                                        <span className="vendor-class"><button onClick={()=>setButtonPopup(true)}>Notify Vendor</button></span>
                                    </div>

                                    <VendorPopup trigger={buttonPopup} setTrigger={setButtonPopup}>
                                        <form>
                                            <div>
                                                <label>Vendor Full Name</label> <br/>
                                                <input type="text" value={firstname} readOnly/>
                                            </div> <br/>

                                            <div>
                                                <label>ATM Status</label> <br/>
                                                <input type="text" value={state_name} readOnly/>
                                            </div> <br/>

                                            <div>
                                                <label>Email</label> <br/>
                                                <input type="text" value={email} readOnly/>
                                            </div> <br/>

                                            <div>
                                                <label>Terminal ID</label> <br/>
                                                <input type="text" value={atm_id} readOnly/>
                                            </div> <br/>

                                            <div>
                                                <label>Message</label> <br/>
                                                <textarea placeholder="Message"></textarea>
                                            </div> <br/>
                                            <div>
                                               
                                               <button onClick={vendorNotification}>Send Message</button>
                                            </div>



                                        </form>
                                    </VendorPopup>
                                    <div className="atm-other-contents">
                                        <ul>
                                            <li><b>Company Name:</b> {company_name}</li>
                                            <li><b>Email:</b> {email}</li>
                                            <li><b>Phone:</b> {phone}</li>
                                            <li><b>Contact Person:</b> {firstname} {lastname}</li>
                                            <li><b>Region:</b> {region}</li>
                                            <li><b>State:</b> {state}</li>
                                            <li><b>Branch:</b> {branch}</li>
                                            <li><b>Vendor ID:</b> {Displayvendors.id}</li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="down-content-page">
                            <div className="down-content-page-list">
                                <div className="device-details">
                                    <p>Device  Details</p>
                                    <div className="device-scroll-detailes-one">

                                        <div>Device Name</div>
                                        <div>Status</div>
                                    </div>
                                    <hr />


                                    <div className="device-details-scroll">
                                        {
                                            Object.keys(Displaydevices).map(Displaydevice => {
                                                return (
                                                    <div key={Displaydevice} className="device-scroll-detailes-one">
                                                        <div className="device_status">{Displaydevices[Displaydevice].device_name}</div>-
                                                        <div className="state-status">{Displaydevices[Displaydevice].state_name}</div>
                                                    </div>
                                                )

                                            })
                                        }
                                    </div>

                                </div>
                                <div className="cash-details">
                                    <p>Cash  Details</p>
                                    <div className="device-scroll-detailes-one">
                                        <div>Device Name</div>
                                        <div>Status</div>
                                    </div>
                                    <hr />
                                    <div className="device-details-scroll">
                                        <div className="cash-detailss"><span className="cash-details-name">Current Ref:</span>-<span className="cash-stausss">{curref}</span></div>
                                        <div className="cash-detailss"><span className="cash-details-name">Cassette Value:</span>-<span className="cash-stausss">{cassette_value}</span></div>
                                        <div className="cash-detailss"><span className="cash-details-name">Cassette Status:</span>-<span className="cash-stausss">{cassette_status}</span></div>
                                        <div className="cash-detailss"><span className="cash-details-name">Notes Initial:</span>-<span className="cash-stausss">{notes_initial}</span></div>
                                        <div className="cash-detailss"><span className="cash-details-name">Notes Left:</span>-<span className="cash-stausss">{notes_left}</span></div>
                                        <div className="cash-detailss"><span className="cash-details-name">Notes Dispensed:</span>-<span className="cash-stausss">{notes_dispensed}</span></div>
                                        <div className="cash-detailss"><span className="cash-details-name">Notes Rejected:</span>-<span className="cash-stausss">{notes_rejected}</span></div>
                                        <div className="cash-detailss"><span className="cash-details-name">Notes Adjusted:</span>-<span className="cash-stausss">{notes_adjusted}</span></div>
                                        {/*  <div className="cash-detailss"><span>region:</span><span>{region}</span></div>  */}
                                        <div className="cash-detailss"><span className="cash-details-name">Previous Notes Left:</span>-<span className="cash-stausss">{previous_notes_left}</span></div>
                                        <div className="cash-detailss"><span className="cash-details-name">Date Modified:</span>-<span className="cash-stausss">{date_modified}</span></div>

                                    </div>
                                </div>
                                <div className="media-details">
                                    <p>Media  Details</p>
                                    <div className="device-scroll-detailes-one">
                                        <div>Device Name</div>
                                        <div>Status</div>
                                    </div>
                                    <hr />
                                    <div className="device-details-scroll">

                                        {
                                            Object.keys(Displaymedias).map(Displaymedia => {
                                                return (
                                                    <div key={Displaymedia} className="device-scroll-detailes-one">


                                                        <div className="device_status">{Displaymedias[Displaymedia].device_name}</div>-
                                                        <div className="state-status">{Displaymedias[Displaymedia].state_name}</div>
                                                    </div>
                                                )

                                            })
                                        }


                                    </div>
                                </div>
                            </div>

                            <div className="chat-page">
                                <div className="inservice">
                                    <h4>In Service</h4>
                                    <h5>Last 24 Hours</h5>
                                    <center>
                                        <div className="char-design">
                                            <DoughnutChart />
                                        </div>
                                    </center>
                                    <div class="dot-dot">
                                        <div className="left-dot">
                                            <div className="left-dot-divide"><div className="inner-left-dot-divide"></div><div>In Service</div></div>
                                            <div className="left-dot-divide"><div className="inner-left-left-dot-divide"></div><div>Lost Comm</div></div>
                                        </div>
                                        <div className="right-dot-divide"><div className="inner-right-dot-divide"></div><div>In Service</div></div>

                                    </div>
                                </div>
                                <div className="cash-status">
                                    <h4 className="font-bold">Cash Status</h4>
                                    <h5>Total Remaining Cash Amount: 10,000,345</h5>
                                    <center>
                                        <div className="char-design">
                                            <DoughnutChart />
                                        </div>
                                    </center>
                                    <div class="dot-dot">
                                        <div className="left-dot">
                                            <div className="left-dot-divide"><div className="inner-left-dot-divide"></div><div>Cash Available</div></div>
                                            <div className="left-dot-divide"><div className="inner-left-left-dot-divide"></div><div>Cash Flow</div></div>
                                        </div>
                                        <div className="right-dot-divide"><div className="inner-right-dot-divide"></div><div>Out of Cash</div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="linechatpage">
                            <div className="doughout-div">
                                <div className="cash-dispense">Cash Dispensed</div><div className="select-days">
                                    <select>
                                        <option>7 Days</option>
                                    </select>
                                </div>
                            </div>
                            <LineChart />
                        </div>
                    </div>
                </div>
            )}

        </div>
    )
}
export default Atms


