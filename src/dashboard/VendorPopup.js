import React from 'react'
import { Formik, Form, Field } from 'formik'

function VendorPopup(props) {
    return (props.trigger) ? (
        <div className="VendorPopup">
            <div className="VendorPopup-inner">
                <button className="close-btn" onClick={() => props.setTrigger(false)}>x</button>
                <br/>
                <hr/>
                {props.children}
            </div>
        </div>
    ) : "";
}

export default VendorPopup
