import React from 'react'
import terminoxlogo from "../logo2old.png";
import "../App.css";
import avatar5 from "../picture.png";
import { Link} from 'react-router-dom';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Nav from './Nav';
import DoughnutChart from '../DoughnutChart'
import LineChart from '../LineChart';
import {Formik, Field, Form,FormikProps} from 'formik'

function AtmInventory() {
    return (
        <div>    
            <div className="main-navigation">
                <div className="left-navigation-link">
                    <Nav/> 
                </div>  
                <div className="right-navigation-link-atm">
                    <div className="flex-er">
                        <p className="dashoboard-text mb-10">ATM Inventory</p>
                         <p className="inventory-button"><span className=""><img src="https://img.icons8.com/ios-glyphs/50/ffffff/add.png"/></span><span>ADD ATM</span></p>
                    </div>

                    
                    <div className="searching-tab mb-10"> 
                        <div>
                            {/* <Field component="search" autoComplete="off"> */}
                        
                        </div>
                        <div className="import-export">
                            <div className="export"><span><img src="https://img.icons8.com/fluent-systems-filled/48/000000/export.png"/></span>Export</div>
                            <div className="import"><span><img src="https://img.icons8.com/fluent-systems-filled/48/000000/import.png"/></span>Import</div>
                            <div className="more"><span><img src="https://img.icons8.com/fluent-systems-filled/48/000000/more.png"/></span>More</div>
                        </div>
                    </div>

                    <div className="table">
                            <table>
                                <tr>
                                    <th>ATM ID</th>
                                    <th>Model</th>
                                    <th>Make</th>
                                    <th>Vendor</th>
                                    <th>Status</th>
                                    <th>Updated</th>
                                    <th>Action</th>
                                    <th>View</th>
                                </tr>

                                 <tr>
                                    <td>234567890</td>
                                    <td>HYSOSUNG</td>
                                    <td>Hyosung Halo</td>
                                    <td>Zercom Systems</td>
                                    <td>Active</td>
                                    <td>Today</td>
                                    <td>Action</td>
                                    <td>View</td>
                                </tr>

                                 <tr>
                                    <td>234567890</td>
                                    <td>GENMEGA</td>
                                    <td>Hyosung Halo</td>
                                    <td>Enyata</td>
                                    <td>Active</td>
                                    <td>Today</td>
                                    <td>Action</td>
                                    <td>View</td>
                                </tr>

                                 <tr>
                                    <td>234567890</td>
                                    <td>HANTLE</td>
                                    <td>Hyosung Halo</td>
                                    <td>Outsourcing Nigeria</td>
                                    <td>Inactive</td>
                                    <td>May 23, 2021</td>
                                    <td>Action</td>
                                    <td>View</td>
                                </tr>

                                 <tr>
                                    <td>234567890</td>
                                    <td>Model</td>
                                    <td>Model</td>
                                    <td>Vendor</td>
                                    <td>Status</td>
                                    <td>Updated</td>
                                    <td>Action</td>
                                    <td>View</td>
                                </tr>
                                <tr>
                                    <td>234567890</td>
                                    <td>Model</td>
                                    <td>Model</td>
                                    <td>Vendor</td>
                                    <td>Status</td>
                                    <td>Updated</td>
                                    <td>Action</td>
                                    <td>View</td>
                                </tr>
                                <tr>
                                    <td>234567890</td>
                                    <td>Model</td>
                                    <td>Model</td>
                                    <td>Vendor</td>
                                    <td>Status</td>
                                    <td>Updated</td>
                                    <td>Action</td>
                                    <td>View</td>
                                </tr>
                                <tr>
                                    <td>234567890</td>
                                    <td>Model</td>
                                    <td>Model</td>
                                    <td>Vendor</td>
                                    <td>Status</td>
                                    <td>Updated</td>
                                    <td>Action</td>
                                    <td>View</td>
                                </tr>
                                <tr>
                                    <td>234567890</td>
                                    <td>Model</td>
                                    <td>Model</td>
                                    <td>Vendor</td>
                                    <td>Status</td>
                                    <td>Updated</td>
                                    <td>Action</td>
                                    <td>View</td>
                                </tr>

                               
                                
                            </table>
                    </div>


                   
                </div>
            </div>
        </div>  
    )
}
export default AtmInventory


