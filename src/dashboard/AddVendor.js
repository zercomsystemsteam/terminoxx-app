import React, { Component } from 'react'
import terminoxlogo from "../logo2old.png";
import "../App.css";
import avatar5 from "../picture.png";
import { Link} from 'react-router-dom';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Nav from './Nav';
import DoughnutChart from '../DoughnutChart'
import LineChart from '../LineChart';
import {Formik, Field, Form,FormikProps} from 'formik'
import { component } from 'react';
import { useState } from 'react';
import axios from 'axios';
const BaseUrl="https://8daeb498fad0.ngrok.io/api/v1.1/region";
class AddVendor extends Component{
   
    state={
        branchLists:[],
        company_name:"",
        lastname:"",
        email:"",
        phone:""
    }
    handleSubmit=(e)=>{
        this.setState({
            
            /* region:e.target.value,
            state:e.target.value,
            branch:e.target.value,
            company_name:e.target.value,
            lastname:e.target.value,
            email:e.target.value,
            phone:e.target.value */
        })
    }
   
    componentDidMount(){
        const getToken=sessionStorage.getItem("token");
        const headers = { 
            'Content-Type':'application/json',
            'Authorization': `Bearer ${getToken}`
        };
        axios.get(BaseUrl,{headers})
        .then(response=>response.data)
        .then((data)=>{
            this.setState({branchLists:data.data})
        })
    }
  
  render(){
    return (
        <div>   
            <div className="main-navigation">
                <div className="left-navigation-link">
                    <Nav/> 
                </div>  
                <div className="right-navigation-link-atm">
                    <p className="dashoboard-text mb-10">Add New Vendor</p>
                    <hr/>
                    <div className="Add-atm-form">
                        <Formik>
                            <Form>
                            <div className="Long-formik-input">
                                    <label>Region:</label> <br/>
                                     <Field component="select" name={this.state.region} onChange={this.handleSubmit}>
                                     { 
                                        this.state.branchLists.map((branchList)=>(
                                                <option>{branchList.region_name}</option>
                                        )) 
                                     }
                                     </Field>
                                </div>
                                <div className="Long-formik-input">
                                     <label>State:</label><br/>
                                     <Field component="select" name={this.state.name}>
                                                <option>Please Select</option>
                                     </Field>
                                </div>
                                <div className="Long-formik-input">
                                    <label>Branch:</label> <br/>
                                     <Field component="select" name={this.state.branch}>
                                                <option>Please Select</option>
                                     </Field>
                                </div>
                                <div className="Long-formik-input">
                                <label>Company Name:</label>
                                    <Field autoComplete="off" name={this.state.company_name} placeholder="Company Name"/>
                                </div>
                                 <div className="divide-one-formik-input">
                                    <div className="model-make">
                                        <label>First Name:</label> <br/>
                                            <Field autoComplete="off" name={this.state.first_name} placeholder="First Name"/>
                                    </div>
                                     <div className="model-make">
                                        <label>Last Name:</label> <br/>
                                        <Field autoComplete="off" name={this.state.last_name} placeholder="last Name"/>
                                    </div>
                                </div>

                                 <div className="Long-formik-input">
                                <label>Email:</label> <br/>
                                     <Field name={this.state.email}/>             
                                </div>

                                <div className="Long-formik-input">
                                <label>Phone Number:</label>
                                     <Field name={this.state.phone}/>             
                                </div>
                                <div>
                                       <button className="enrol-vendor">Enroll Vendor</button>
                                    </div> 
                            </Form>
                        </Formik>
                    </div>
                </div>
            </div>
        </div>  
    )
}
}
export default AddVendor


