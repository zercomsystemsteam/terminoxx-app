import React from 'react'
import terminoxlogo from "../logo2old.png";
import "../App.css";
import avatar5 from "../picture.png";
import { Link} from 'react-router-dom';

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Nav from './Nav';
import DoughnutChart from '../DoughnutChart'
import LineChart from '../LineChart';
import {Formik, Field, Form,FormikProps} from 'formik'

function AddUsers() {
  
    return (
        <div>    
            <div className="main-navigation">
                <div className="left-navigation-link">
                    <Nav/> 
                </div>  
                <div className="right-navigation-link-atm">
                    <p className="adduser-div">Add Users</p>

                    <div className="Add-atm-form">
                        <Formik>
                            <Form>
                                 <div className="divide-one-formik-input">
                                    <div className="model-make">
                                        <label>Surname:</label> <br/>
                                            <Field autoComplete="off" name="ATM ID" placeholder="First Name"/>
                                    </div>

                                     <div className="model-make">
                                        <label>Last Name:</label> <br/>
                                        <Field autoComplete="off" name="ATM ID" placeholder="Last Name"/>
                                    </div>
                                </div>

                                {/* <div className="Long-formik-input">
                                <label>Email:</label>
                                     <Field name="ATM ID" placeholder="ex:myname@example.com"/>             
                                </div> */}

                                {/* <div className="Long-formik-input">
                                <label>Role:</label>
                                     <Field component="select" name="ATM ID">
                                                <option>Please Select</option>
                                     </Field>
                                </div> */}
                                {/* <div className="divide-one-formik-input">
                                    <div className="model-make">
                                        <label>Password:</label> <br/>
                                            <Field name="ATM ID" placeholder="Enter Password"/>  
                                    </div>

                                     <div className="model-make">
                                        <label>Confirm Password:</label> <br/>
                                        <Field name="ATM ID" placeholder="Confirm Password"/> 
                                    </div>   
                                </div> */}
                                {/* <div>
                                    <button className="bg-red-700 pt-4 pb-4 text-white w-80 mt-10 mb-10 rounded">Add User </button>
                                </div>  */}

                                
                            </Form>
                        </Formik>
                    </div>
                </div>
            </div>
        </div>  
    )
}
export default AddUsers


