import React from 'react'
import terminoxlogo from "../logo2old.png";
import whitelogo from '../Img/whitelogo.png';
import "../App.css";
import avatar5 from "../picture.png";
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { removeUserSession } from '../includes/Controller';
import { getUser } from '../includes/Controller';

const Nav = () => {
    const user = getUser();
    const history = useHistory();
    const handleLogout = () => {
        alert("Logout Successfully")
        removeUserSession("data", "token")
        history.push("/pages/Adminlogin");
    }

    /* const state={
        divcontainer:false,
    } */

    /*    const Handlechange = e =>
           {
               this.setState({divcontainer:!this.state.divcontainer});
           }
           const nav=this.state.divcontainer; */

    if (user.role == "Super Admin") {
        return (
            <div>
                <div class="nav-header">
                    <div className="logo-nav">
                        <span className="first-logo"><img src={whitelogo} className="logo-img" /></span>
                        <span className="hide-icons"><img src="https://img.icons8.com/ios-filled/50/ffffff/moleskine-icon.png" /></span>
                    </div>
                    <div className="first-logo-date">
                        <span>Last Update: 4/10/2021, 8:15:30 PM <span className="role-name">{user.role}</span></span>
                        <span><img src="https://img.icons8.com/ios/50/ffffff/appointment-reminders.png" alt="notification Bell" /></span>
                        <span className="user-profile"><img src={avatar5} /></span>
                    </div>
                </div>


                <div className="main-navigation">

                    <div className="left-navigation-link">
                        <div className="navigations-dashboard"><Link to="Index"><span><img src="https://img.icons8.com/ios-filled/50/ffffff/home.png" alt="hidden icon" /></span><span>Dashboard</span></Link></div>
                        <div className="left-navigation-link-ul">

                            <div className="navigations"><Link to="Statistics"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/statistics.png" alt="statistics icon" /></span>Statistics</Link></div>
                            <div className="navigations"><Link to="Atms"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/atm.png" alt="atm icon" /></span>ATMs</Link></div>
                            <div className="navigations"><Link to="Custodians"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/home.png" alt="custodians icon" /></span>Custodians</Link></div>
                            <div className="navigations"><Link to="Branches"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/deciduous-tree.png" alt="Branches icon" /></span>Branches</Link></div>
                            <div className="navigations"><Link to="Regional-Manager"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/manager.png" alt="Regional Managers icon" /></span>Regional Managers</Link></div>
                            <div className="navigations"><Link to="AddVendor"><span><img src="https://img.icons8.com/ios-glyphs/30/cccccc/food-truck.png" alt="vendor icon" /></span>Vendors</Link></div>
                            <div className="navigations"><Link to="Administrator"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/administrator-male.png" alt="Administrators icon" /></span>Administrators</Link></div>
                            <div className="navigations"><Link to="App-Administrator"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/home.png" alt="App Administrator icon" /></span>App Administrators</Link></div>
                            <div className="navigations"><Link to="Subscribers"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/follow.png" alt="Subscribers icon" /></span>Subscribers</Link></div>
                            <div className="navigations"><Link to="Update-atm"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/approve-and-update.png" alt="Updates icon" /></span>Update ATMs</Link></div>
                            <div className="navigations"><Link to="AtmInventory"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/inventory-flow.png" alt="ATM inventory icon" /></span>ATM Inventory</Link></div>
                            <div className="navigations"><Link to="Settings"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/settings.png" alt="Settings" /></span>Settings</Link></div>
                            <div className="navigations" onClick={handleLogout}><Link to="#"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/logout-rounded-left.png" alt="Logout icon" /></span>Logout</Link></div>
                        </div>
                    </div>

                </div>
            </div>
        )
    } else {
        return (
            <div>
                <div class="nav-header">
                    <div className="logo-nav">
                        <span className="first-logo"><img src={terminoxlogo} className="logo-img" /></span>
                        <span className="hide-icons"><img src="https://img.icons8.com/ios-filled/50/ffffff/moleskine-icon.png" /></span>
                    </div>
                    <div className="first-logo-date">
                        <span>Last Update: 4/10/2021, 8:15:30 PM <span className="role-name">{user.role}</span></span>
                        <span><img src="https://img.icons8.com/ios/50/ffffff/appointment-reminders.png" alt="notification Bell" /></span>
                        <span className="user-profile"><img src={avatar5} /></span>
                    </div>
                </div>


                <div className="main-navigation">

                    <div className="left-navigation-link">
                        <div className="navigations-dashboard"><Link to="Index"><span><img src="https://img.icons8.com/ios-filled/50/ffffff/home.png" alt="hidden icon" /></span><span>Dashboard</span></Link></div>
                        <div className="left-navigation-link-ul">

                            <div className="navigations"><Link to="Statistics"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/statistics.png" alt="statistics icon" /></span>Statistics</Link></div>
                            <div className="navigations"><Link to="Atms"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/atm.png" alt="atm icon" /></span>ATMs</Link></div>
                            <div className="navigations"><Link to="Custodians"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/home.png" alt="custodians icon" /></span>Custodians</Link></div>
                            <div className="navigations"><Link to="Branches"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/deciduous-tree.png" alt="Branches icon" /></span>Branches</Link></div>
                            <div className="navigations"><Link to="Regional-Manager"><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/manager.png" alt="Regional Managers icon" /></span>Regional Managers</Link></div>

                            <div className="navigations" onClick={handleLogout}><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/logout-rounded-left.png" alt="Logout icon" /></span>Logout</div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }







}
export default Nav
