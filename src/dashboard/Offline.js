import React from 'react'
import "../App.css";
import dispencing from '../Img/dispencing.png';
import not_dispencing from '../Img/not_dispencing.png';
import out_service from '../Img/out_service.png';
import { Component } from 'react';
import Nav from './Nav';
import AtmAvailability from '../AtmAvailability';
import axios from 'axios';
import { getToken } from '../includes/Controller';
import { useSelector } from 'react-redux';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setAtms } from '../redux/actions/atmActions';
import { render } from '@testing-library/react';
import { Link } from 'react-router-dom';
//import { useState } from 'react';

const Offline = () => {
    const dispatch = useDispatch();
    const getToken = sessionStorage.getItem("token");
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${getToken}`
    };
    const fetchAllAtms = async () => {
        const response = await axios.get("https://721f-38-107-67-161.ngrok.io/api/v1.1/atm_oracle", { headers }).catch((err) => {
            console.log("Err", err)
        });
        dispatch(setAtms(response.data.data));
    };
    useEffect(() => {
        fetchAllAtms();
    }, [])

    const atms = useSelector((state) => state.allAtms.atms);
    const renderList = atms.map((atm) => {
        const { id, atm_id, state_name, citystate,actual_op_state } = atm;


        {
            if (actual_op_state == "2") {
                return (
                    <div className="atmlistt" key={atm_id}>
                        <Link to={`atm/${atm_id}`}><div className="dispending-div">
                            <center><h4 className="city">{atm_id}, {citystate}</h4>
                                <h4>{state_name}</h4></center>
                        </div>
                        </Link>
                    </div>
                )
            }

        }

    });
    return (
        <div>
            {renderList}
        </div>

    )
}
export default Offline;
