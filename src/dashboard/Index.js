import React from 'react'
import "../App.css";
import dispencing from '../Img/dispencing.png';
import not_dispencing from '../Img/not_dispencing.png';
import out_service from '../Img/out_service.png';
import { Component } from 'react';
import Nav from './Nav';
import AtmAvailability from '../AtmAvailability';
import axios from 'axios';
import { getToken } from '../includes/Controller';
import { useSelector } from 'react-redux';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';


import AtmList from './AtmList';
import Offline from './Offline';
import OutofService from './OutofService';
//import { useState } from 'react';


const Index = (props) => {
    const atms = useSelector((state) => state.allAtms.atms);
    const dispatch = useDispatch();

    return (
        <div>

            <div className="main-navigation">
                <div className="left-navigation-link">
                    <Nav />
                </div>
                <div className="right-navigation-link">
                    <div className="first-line-main">
                        <div className="first-line-home">
                            <div className="active-atm">
                                <div><span className="atm-active-number">{/* {props.state.atm_id} */} 740</span><br /><span className="atm-active-status">ATMS INSERVICE</span></div>
                                <div><img src={dispencing} alt="round image" className="active-image" /></div>
                            </div>
                            <div className="atm-not-dispensing">
                                <div><span className="atm-active-dispensing">86</span><br /><span className="atm-dispensing-status">ATMS NOT DISPENSING</span></div>
                                <div><img src={not_dispencing} alt="round image" className="dispencing-active-image" /></div>
                            </div>
                            <div className="atm-outservice">
                                <div><span className="atm-active-outservice">33</span><br /><span className="atm-outservice-status">ATMS OUT OF SERVICE</span></div>
                                <div><img src={out_service} alt="round image" className="outservice-active-image" /></div>
                            </div>
                        </div>

                       


                        <div className="second-line-home">
                            <p className="atm-list">ATM LIST</p>
                            <div className="atm-list-item">
                                <div className="atm-list-scroll">
                                    <p>ATM ONLINE</p>
                                    <div>
                                        <div className="dispensing">
                                            <div className="dispensing-tabs">
                                                <AtmList />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="atm-list-scroll"><p>ATM OFFLINE</p>
                                    <div className="not-dispensing">
                                        <div className="dispensing-tabs" id="offline-atm">
                                            <Offline />
                                        </div>
                                    </div>
                                </div>
                                <div className="atm-list-scroll"><p>ATM OUT OF SERVICE</p>
                                    <div className="out-service">
                                        <div className="dispensing-tabs">
                                            <OutofService />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="second-line">
                        <div className="second-first-line">
                            <h3>ATM AVAILABILITY</h3>
                            <div className="status-level">
                                <div className="level-icon-tab"><span className="level-icons"></span>in Service</div>
                                <div className="level-icon-tab"><span className="level-inservice"></span>Out of Service</div>
                                <div className="level-icon-tab"><span className="level-dispensine"></span>Not Dispensing</div>
                            </div>
                            <center>
                                <div className="doughout">
                                    <AtmAvailability />
                                </div>
                            </center>

                            <div>
                                <div className="counters"><span>In Service</span><span className="counter-service">90%</span></div>
                                <div className="counters"><span>Out of Service</span><span className="counter-out">30%</span></div>
                                <div className="counters"><span>Not Dispensing</span><span className="counter-dispensine">10%</span></div>
                            </div>
                        </div>
                        <div className="second-second-line">
                            <h3>CUMMULATIVE ANALYTICS</h3>
                            <select>
                                <option>24 Hours</option>
                            </select>

                            <div className="status-level">
                                <div className="level-icon-tab"><span className="level-icons"></span>in Service</div>
                                <div className="level-icon-tab"><span className="level-inservice"></span>Out of Service</div>
                                <div className="level-icon-tab"><span className="level-dispensine"></span>Not Dispensing</div>
                            </div>
                            <center>
                                <div className="doughout">
                                    <AtmAvailability />
                                </div>
                            </center>
                            <div>
                                <div className="counters"><span>In Service</span><span className="counter-service">90%</span></div>
                                <div className="counters"><span>Out of Service</span><span className="counter-out">30%</span></div>
                                <div className="counters"><span>Not Dispensing</span><span className="counter-dispensine">10%</span></div>
                            </div>
                        </div>
                        <div className="second-third-line">
                            <div className="admin-counteres">
                                <div className="first-admin-counteres"><div className="top-admin-counteres"><center><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/administrator-male.png" alt="Administrators icon" /></span></center></div><div className="bottom-admin-counteres">704</div></div>
                                <div className="first-admin-counteres"><div className="top-admin-counteres"><center><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/administrator-male.png" alt="Administrators icon" /></span></center></div><div className="bottom-admin-counteres">581</div></div>
                                <div className="first-admin-counteres"><div className="top-admin-counteres"><center><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/administrator-male.png" alt="Administrators icon" /></span></center></div><div className="bottom-admin-counteres">53,003</div></div>
                            </div>
                            <div className="admin-counteres">
                                <div className="first-admin-counteres"><div className="top-admin-counteres"><center><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/administrator-male.png" alt="Administrators icon" /></span></center></div><div className="bottom-admin-counteres">704</div></div>
                                <div className="first-admin-counteres"><div className="top-admin-counteres"><center><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/administrator-male.png" alt="Administrators icon" /></span></center></div><div className="bottom-admin-counteres">581</div></div>
                                <div className="first-admin-counteres"><div className="top-admin-counteres"><center><span><img src="https://img.icons8.com/ios-filled/50/CCCCCC/administrator-male.png" alt="Administrators icon" /></span></center></div><div className="bottom-admin-counteres">53,003</div></div>
                            </div>
                        </div>
                    </div>





                </div>
            </div>
        </div>

    )
}



export default Index
