import { combineReducers } from "redux";
import { atmReducer, selectedAtmReducer, selectedDeviceReducer, selectedMediaReducer, selectedCashReducer, selectedVendorReducer, selectedCustodianReducer } from "./atmReducer";
const reducers = combineReducers({
    allAtms: atmReducer,
    atm: selectedAtmReducer,
    devices: selectedDeviceReducer,
    medias: selectedMediaReducer,
    cashs: selectedCashReducer,
    vendors: selectedVendorReducer,
    custodians: selectedCustodianReducer,
});

export default reducers;




