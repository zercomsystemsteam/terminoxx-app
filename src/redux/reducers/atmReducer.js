import { ActionTypes } from "../constants/action-types";

const initialState = {
    atms: [],
}

/* const deviceInitialState = {
    atms: { device: [] }
} */

export const atmReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case ActionTypes.SET_ATMS:
            return { ...state, atms: payload }

        default:
            return state
    }
}

export const selectedAtmReducer = (state = {}, { type, payload }) => {
    switch (type) {
        case ActionTypes.SELECTED_ATM:
            return { ...state, ...payload };
        default:
            return state;
    }
}

export const selectedDeviceReducer = (state = {}, { type, payload }) => {
    switch (type) {
        case ActionTypes.SELECTED_DEVICE:

            return { ...state, ...payload };

        default:
            return state;
    }
}
export const selectedMediaReducer = (state = {}, { type, payload }) => {
    switch (type) {
        case ActionTypes.SELECTED_MEDIA:

            return { ...state, ...payload };

        default:
            return state;
    }
}

export const selectedCashReducer = (state = {}, { type, payload }) => {
    switch (type) {
        case ActionTypes.SELECTED_CASH:

            return { ...state, ...payload };

        default:
            return state;
    }
}

export const selectedVendorReducer = (state = {}, { type, payload }) => {
    switch (type) {
        case ActionTypes.SELECTED_VENDOR:

            return { ...state, ...payload };

        default:
            return state;
    }
}

export const selectedCustodianReducer = (state = {}, { type, payload }) => {
    switch (type) {
        case ActionTypes.SELECTED_CUSTODIAN:

            return { ...state, ...payload };

        default:
            return state;
    }
}