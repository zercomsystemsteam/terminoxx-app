//import { ActionTypes } from "./constants/action-types";
import { ActionTypes } from "../constants/action-types";

export const setAtms = (atms) => {
    return {
        type: ActionTypes.SET_ATMS,
        payload: atms,
    }
}
export const selectedAtm = (atm) => {
    return {
        type: ActionTypes.SELECTED_ATM,
        payload: atm,
    }
}
export const selectedDevice = (devices) => {
    return {
        type: ActionTypes.SELECTED_DEVICE,
        payload: devices,
    }
}

export const selectedMedia = (medias) => {
    return {
        type: ActionTypes.SELECTED_MEDIA,
        payload: medias,
    }
}

export const selectedCash = (cashs) => {
    return {
        type: ActionTypes.SELECTED_CASH,
        payload: cashs,
    }
}
export const selectedVendor = (vendors) => {
    return {
        type: ActionTypes.SELECTED_VENDOR,
        payload: vendors,
    }
}

export const selectedCustodian = (custodians) => {
    return {
        type: ActionTypes.SELECTED_CUSTODIAN,
        payload: custodians,
    }
}


