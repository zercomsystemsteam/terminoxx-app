import React from 'react'
import whitelogo from './Img/whitelogo.png';
import mainback from './mainback.png';
import {BrowserRouter as Router, Route, Link, Switch} from "react-router-dom";



function Home() {
    return (
        <center>
        <div className="main-front-page" style={{backgroundImage:`url(${mainback})`}}>
          <br />
            <img src={whitelogo} alt="" />
           <h3 className="main-front-page-heading">ATM MONITORING</h3>
            <div className="login-button">
            <Link to="./Pages/Adminlogin"> Login</Link>
            </div>
            {/* <div className="bg-white text-black text-2xl p-4 w-96 mt-10 mb-12 font-bold">
            <Link to="./Pages/ManagerLogin">Manager</Link>
            </div> */}
            <br />

      </div>
      
      </center>
    )
}

export default Home
