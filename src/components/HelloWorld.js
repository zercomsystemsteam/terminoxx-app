import { useState } from "react"
import axios from "axios"

const HelloWorld = () => {
    
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    
    const login = () =>{
        
        const headers = {
            /* 'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:8082/',
            'Access-Control-Request-Method': 'POST',
            'Access-Control-Request-Headers': 'x-token',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
            'Access-Control-Allow-Credentials':'true' */
          }
        
        // axios.post('https://8daeb498fad0.ngrok.io/api/v1.1/admin/login', {
      axios.post('https://8daeb498fad0.ngrok.io/api/v1.1/admin/login', {
        'email': email,
        'password': password
      }, headers)
        .then(response => {
          console.log(response)
          console.log(response.data.token)
          localStorage.setItem("token", response.data.token)
        })
        .catch(e => {
          // console.log(e)
        })
        
        
    }
    return (
        <div>
            <form>
                <label htmlFor="email">Email</label><br />
                <input type="text" value={email} onChange={(e) => setEmail(e.target.value)} /><br/>
                <label htmlFor="password">Password</label><br/>
                <input type="text" value={password} onChange={(e) => setPassword(e.target.value)} /><br />
                <input type="button" value="Login" onClick={login} />
            </form>
            
        </div>
    )
}

export default HelloWorld
