import React from 'react'
import {Doughnut} from 'react-chartjs-2'

function AtmAvailability() {
    const data={
        //labels:['Cash Available','Cash Low','Out of Cash'],
        datasets:[{
            label:'Cash Status',
            data:[8,1,1],
            backgroundColor:[
                '#00A65A',
                '#FFB570',
                '#CD0003'
                ]
                
        }],
       
    }

    const options={
        title:{
            display:true,
            text:'Doughnut Chart',
            cutoutPercentage: 70
        }
    }

    return (
        <div className="chater">
        <Doughnut data={data} options={options}/>
        </div>
    )
}

export default AtmAvailability
