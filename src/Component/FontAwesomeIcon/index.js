import { library } from '@fortawesome/fontawesome-svg-core';
import { faUser, faEnvelope, faLock, faArrowRight, faBell } from '@fortawesome/free-solid-svg-icons'
library.add(faUser, faEnvelope, faLock, faArrowRight, faBell)