import React from 'react'
import {Line} from 'react-chartjs-2'

function LineChart() {
    const data={
        labels:['Tue','Wed','Thur','fri','Sat','Sun','Mon','Tue'],
        datasets:[{
            label:'Cash Dispensed',
            data:[1000000,1800000,1300000,2200000,2600000,1900000,2200000,1500000],
            backgroundColor:['rgba(255,206,86,0.2'],
            pointBackgroundColor:['#CC0003'],
            borderColor:['#CC0003'],
            pointBorderColor:['rgba(255,206,86,0.2'],
            cutout: 80
            
        },
       
        ]
    }

    const options={
        title:{
            display:true,
            text:'Line Chart'
        },
        scales:{
            yAxes:[{
                ticks:{
                    min:0,
                    max:6,
                    stepSize:1
                }
            }]
        }
    }

    return (
        <Line data={data} options={options}/>
    )
}

export default LineChart
