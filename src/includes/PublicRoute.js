import react from 'react';
import { Redirect, Route, route } from 'react-router-dom';
import { getToken } from './Controller';

const PublicRoute=({component:Component, ...rest})=>{
    return(
        <Route
            {...rest}
            render={props=>{
                return !getToken()? <Component{...props}/>:<Redirect to={{pathname:"/dashboard/Index"}}/>;
            }}
        />
    );
}
export default PublicRoute;