export const getUser=()=>{
    const userStr=sessionStorage.getItem("data");
    if(userStr)return JSON.parse(userStr);
    else return null;
}
export const getToken=()=>{
    return sessionStorage.getItem("token")|| null;
    //sessionStorage.getItem("data");
}
export const setUserSession=(token, data)=>{
    sessionStorage.setItem("token", token);
    sessionStorage.setItem("data", JSON.stringify(data));

}
export const removeUserSession=()=>{
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("data");
}