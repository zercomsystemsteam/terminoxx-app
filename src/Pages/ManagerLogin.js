
//import './App.css';
import background from "../background.jpg"
import terminoxlogo from "../terminoxlogo.png";
import logo2old from "../logo2old.png"
import {Formik, Form, Field, ErrorMessage, ValidateYupScheme} from "formik";
import * as Yup from "yup";
import { Link } from 'react-router-dom';
import axios from 'axios';
import { constant } from 'lodash';
import React, { useState } from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

 const yup = require('yup')
require('yup-password')(yup) 

function ManagerLogin() {
    //const [loginMsg, setLoginMsg]=useState('');
    const initialValues={
        Email:"",
        Password:""
    }
    const validationSchema=Yup.object().shape({
        Email:Yup.string().email(),
        Password:Yup.string().min(6).minUppercase(1).minSymbols(1).required()
    });
    const input = {
        password: 'secret'
    }



    return (
        <div>
            <div className="w-full h-auto bg-cover bg-no-repeat ba" style={{backgroundImage: `url(${background})`}}>
            <div className="w-full justify-items-center items-center flex flex-col">
                    <div className="mt-10 bg-white rounded-xl p-10 mb-4">
                        <center><img src={terminoxlogo} alt="Terminoxx logo" className="w-72"/></center>
                        <p className="mt-4 font-bold text-black text-center">for Regional Managers</p>
                        <div className="mt-4">
                            <Formik initialValues={initialValues} /* onSubmit={onSubmit} */ validationSchema={validationSchema}>
                            <Form>     
                            <div>
                                    <ErrorMessage name="Email" component="span" className="text-red-600"/><br/>
                                    <div className="bg-red-700 h-14 w-2 mt-4 absolute">        
                                    </div>
                                    <Field autoComplete="off" name="Email" Placeholder="Email" className="mt-4 shadow-inner w-96 h-14 border rounded p-2"/>
                                    <span className="relative -ml-10">
                                            <FontAwesomeIcon icon="envelope" className="text-gray-400"/>
                                    </span>
                                </div>
                                
                                <div>
                                <ErrorMessage name="Password" component="span" className="text-red-600"/> <br/>
                                <div className="bg-red-700 h-14 w-2 mt-4 absolute"></div>      
                                <Field autoComplete="off" id="" type="password" name="Password" Placeholder="Password" className="mt-4 shadow-inner w-96 h-14 border rounded p-2"/>
                                    <span className="-ml-10 relative">
                                            <FontAwesomeIcon icon="lock" className="text-gray-400"/>
                                    </span>  
                                    <br/>
                                    <span className="float-right mt-2 text-secondary-100"><Link to="./Adminlogin">Switch to Administrator</Link></span>
                                </div>
                                <br/>
                                <div>
                                     <button type="submit" className="w-96 bg-red-700 h-14 text-white mt-12 mb-10 text-2xl">Sign In</button>
                                     <span className="-ml-10 relative">
                                            <FontAwesomeIcon icon="arrow-right" className="text-white"/>
                                    </span> 

                                    <div>
                                       <Link to="/dashboard/index">Testing</Link> 
                                    </div>
                                 </div>
                            </Form>
                        </Formik>
                        </div>
                    

                    </div>
                    
                </div>
                </div>       
        </div>
    )
}

export default ManagerLogin
