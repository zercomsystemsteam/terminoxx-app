
//import './App.css';
import background from "../Img/background.jpg";
import blacklogos from '../Img/blacklogos.png';



import {Formik, Form, Field, ErrorMessage, /* ValidateYupScheme */} from "formik";
import * as Yup from "yup";
import { Link } from 'react-router-dom';
import axios from 'axios';
import React, { useState } from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { useHistory } from 'react-router-dom';
import { setUserSession } from "../includes/Controller";

 const yup = require('yup')
    require('yup-password')(yup) 

/* const myChangeHandler = (event) => {
    this.setState({email: event.target.value});
    this.setState({password: event.target.value});
  } */

    const Adminlogin=(props)=>{
    const [email, setEmail]=useState("");
    const [password,setPassword]=useState("");
    const[loading, setLoading]=useState(false);
    const [error, setError]=useState(null);
    const [loginMsg, setLoginMsg]=useState('');
    let history=useHistory();
    const handleLogin = () =>{
      const data = { email: email, password: password };
      setLoading(true);
      setError(null);
      axios.post('https://721f-38-107-67-161.ngrok.io/api/v1.1/admin/login',data)
        .then(response => {
          if(response.data.status=="Wrong login details"){
            setLoading(false);
            setError(null);
            alert("Wrong login details");
            props.history.push("/")
          }else{   
            setLoading(false);
            setError(null);
            alert("You Have successfully Logged In");
            setUserSession(response.data.token, response.data.data);
            history.push('/dashboard/Index');
          }
          
        })
        .catch(error => {
          if(error.response.status===401 || error.response.status===400){
            alert("Oops! Something Went Wrong, Please Check Your Login Credential and Try Again");
          console.log(error)
          }else{
            alert("Oops! Something Went Wrong, Please Try Again Later");
            props.history.push("/")
          }
        })  
    }


    const initialValues={
        email:"",
        password:""
    }
    const validationSchema=Yup.object().shape({
        email:Yup.string().email().required(),
        password:Yup.string().min(6).minUppercase(1).minSymbols(1).required()
    });
    const input = {
        password: 'secret'
    }

    return (
        <div>
            <div className="login-page-interface" style={{backgroundImage: `url(${background})`}}>
            <br/>
            <div className="flogin-form-pagea">
                    <div className="flogin-form-page">
                        <center><img src={blacklogos} alt="Terminoxx logo" className="w-72"/></center>
                        <p className="mt-4 font-bold text-black text-center">for Administrators</p>
                        <div className="form-login">
                            <Formik initialValues={initialValues} /* onSubmit={onSubmit} */ validationSchema={validationSchema}>
                            <Form>     
                                {/* <svg className="fill-current h-4 w-4 mt-16 marginright absolute" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg> */}                          
                                <div>
                                    <div className="bg-red-700 h-14 w-2 mt-4 absolute">        
                                    </div>
                                    <Field autoComplete="off" name="email" value={email} onChange={(e)=>setEmail(e.target.value)} Placeholder="Email" className="mt-4 shadow-inner w-96 h-14 border rounded p-2"/>
                                    <span className="icon-image">
                                            <FontAwesomeIcon icon="envelope" className="text-gray-400"/>
                                    </span> <br/>
                                    <ErrorMessage name="email" component="span" className="text-red-600"/>
                                </div>
                                <div>
                                <div className="bg-red-700 h-14 w-2 mt-4 absolute"></div>      
                                <Field autoComplete="off" id="" type="password" name="password" value={password} onChange={(e)=>setPassword(e.target.value)} Placeholder="Password" className="mt-4 shadow-inner w-96 h-14 border rounded p-2"/>
                                    <span className="icon-image">
                                            <FontAwesomeIcon icon="lock" className="text-gray-400"/>
                                    </span>   <br/>
                                    <ErrorMessage name="password" component="span" className="text-red-600"/>
                                    <br/>
                                    
                                </div>
                                
                                <br/>
                                
                                  <div className="signbuttondiv">
                                     <button type="submit" disabled={loading} onClick={handleLogin} value={loading ? "loading---":"Adminlogin"} className="signinbtn">Sign In</button>
                                    
                                     <span className="relative">
                                            <FontAwesomeIcon icon="arrow-right" className="text-white"/>
                                    </span> 
                                 </div>
                                 <center><span className="float-right mt-2 text-secondary-100"><Link to="/">Home</Link></span></center>

                                 {error &&<center><div className="error">{error}</div></center>}
                            </Form>
                        </Formik>
                        </div>
                    

                    </div>
                    
                </div>
                </div>  
                    
        </div>
    )
};

export default Adminlogin


