import logo2old from './logo2old.png';
import mainback from './mainback.png';
import "./Component/FontAwesomeIcon";
import {BrowserRouter as Router, Route, Link, Switch} from "react-router-dom";
//import { Link } from 'react-router-dom';
import './App.css';
import ManagerLogin from './Pages/ManagerLogin';
import AdminLogin from './Pages/Adminlogin';
import Home from './Home';
import Index from './dashboard/Index';
import Statistics from './dashboard/Statistics';
import Atms from './dashboard/Atms';
import Custodians from './dashboard/Custodians';
import DoughnutChart from './DoughnutChart'
import LineChart from './LineChart'
import AtmAvailability from './AtmAvailability'
import AddAtm from './dashboard/AddAtm'
import AddUsers from './dashboard/AddUsers'
import AddVendor from './dashboard/AddVendor'
import AtmInventory from './dashboard/AtmInventory'
import HelloWorld from './components/HelloWorld';
import PrivateRoute from './includes/PrivateRoute';
import PublicRoute from './includes/PublicRoute';
import { Component, useState } from 'react';
import { getToken } from './includes/Controller';
import { useEffect } from 'react';
import AtmList from './dashboard/AtmList';
import Settings from './dashboard/Settings';


function App() {
  const [authLoading, setAuthLoading]=useState(true);

  
  useEffect(() => {
    const token=getToken();
    if (!token){
      return;
    }
  }, [])

  /* if(authLoading && getToken()){
    return(
      <div className="authtentication">Checking Authentication...</div>
    )
  } */



  return (
    <Router>
    <div className="App">
        <Switch>
        <Route exact path="/">
          <Home/> 
        </Route>
          <Route path="/Pages/ManagerLogin">
            <ManagerLogin/>
          </Route>
          <PublicRoute path="/Pages/Adminlogin" component={AdminLogin} />
          <PrivateRoute path="/dashboard/Index" component={Index}/>
          <Route path="/dashboard/Statistics" component={Statistics}/>
          <Route path="/dashboard/Atms" component={Atms}/>
          <Route path="/dashboard/Custodians" component={Custodians}/>
          <Route path="/DoughnutChart" component={DoughnutChart}/>
          <Route path="/LineChart" component={LineChart}/>
          <Route path="/AtmAvailability" component={AtmAvailability}/> 
          <Route path="/dashboard/AtmList" component={AtmList}/>
          <Route path="/dashboard/atm/:atm_id" component={Atms}/>
          <Route path="/dashboard/AddAtm" component={AddAtm}/>
          <Route path="/dashboard/AddUsers" component={AddUsers}/>
          <Route path="/dashboard/AddVendor" component={AddVendor}/>
          <Route path="/dashboard/AtmInventory" component={AtmInventory}/>
          <Route path="/dashboard/Settings" component={Settings}/>
        </Switch>
    </div>
    </Router>
  );
}

export default App;
