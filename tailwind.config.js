module.exports = {
    purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                primary: "#f1f1f1",
                secondary: {
                    100: "#000000"
                },
                margin: {
                    marginright: "1000px"
                },

                placeholderColor: {
                    'primary': '#3490dc',
                    'secondary': '#ffed4a',
                    'danger': '#e3342f',
                    'marginleft': "30px"
                }


            }


        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}